package requirementGroup.JobHazard;

import org.testng.annotations.Test;

import lib.dataObject.JobHazardData;
import lib.dataObject.LoginData;
import lib.page.JobHazardPage;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import lib.locators.CreateJobHazard;
import lib.locators.CreateJobHazard.JobHazardLocators;
import lib.locators.Yopmail;
import qaframework.Configuration.Config_MobileAndWeb;

public class JobHazardVallidation extends Config_MobileAndWeb {
	
	public void login() throws Exception  {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.LoginNumber).clickSignIn();
		objHome.clickJobHazard();
		jobHazard.clickElementByName(JobHazardLocators.addIcon);		
	}
	
	 public void createJobHazard() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumber, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocation, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes, JobHazardLocators.nearestEmsNotes);		
			jobHazard.clickNext(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazard(objJobHazardData.compassSwitchOrder, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickNext(JobHazardLocators.hotLineNo);
			jobHazard.clickNext(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman, JobHazardLocators.lineman);	
			jobHazard.clickNext(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.apprentice, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions, JobHazardLocators.specialPrecautions);	
			jobHazard.takePhotos();	
			jobHazard.clickNext(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath( JobHazardLocators.workAreaHazard);	
			jobHazard.clickNext(JobHazardLocators.ConfinedCheck);
			jobHazard.clickNext(JobHazardLocators.movingCheck);
			jobHazard.clickNext(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazard(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickNext(JobHazardLocators.workHazardDone);
			jobHazard.clickNext(JobHazardLocators.foruthScreenNext);
			jobHazard.clickNext(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 30-4-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 1, description = "Verify that able to create the report by attaching pictures from Gallery and submit")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Verify that able to create the report by attaching pictures from Gallery and submit";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			login();
			createJobHazard();
			jobHazard.takePicture();
			jobHazard.clickSave();
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);		
		}
	 
	 
   
}
