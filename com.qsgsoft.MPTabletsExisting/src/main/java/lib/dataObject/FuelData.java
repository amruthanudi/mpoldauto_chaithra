package lib.dataObject;
import java.util.Random;

import lib.dataObject.ExpenseData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class FuelData {
	public  FuelData() throws Exception {
		
	}
	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "FuelDataXml";

	public final String jobNumber = xml.read(ConsumerTag.jobNumber, FILEPATH),
			                         receiptDate = xml.read(ConsumerTag.receiptDate, FILEPATH),
			                         vehicleNumber = xml.read(ConsumerTag.vehicleNumber, FILEPATH),
			                        	mileage = xml.read(ConsumerTag.mileage, FILEPATH),
			                        	offLoadGallon = xml.read(ConsumerTag.offLoadGallon, FILEPATH),
			                        	remarks = xml.read(ConsumerTag.remarks, FILEPATH),
			                        	alertForSubmittedReport = xml.read(ConsumerTag.alertForSubmittedReport, FILEPATH),
			                        	alertforPhotos = xml.read(ConsumerTag.alertforPhotos, FILEPATH),
			                        	cameraRoll = xml.read(ConsumerTag.cameraRoll, FILEPATH),
			                        	photoName = xml.read(ConsumerTag.photoName, FILEPATH),
	                                 OK = xml.read(ConsumerTag.OK, FILEPATH);
			
	public static class ConsumerTag {
		public static final String  jobNumber = "jobNumber",
				                                     receiptDate = "receiptDate",
				                                     vehicleNumber = "vehicleNumber",
				                                     mileage = "mileage",
				                                     offLoadGallon = "offLoadGallon",
				                                     remarks = "remarks",
				                                     alertForSubmittedReport = "alertForSubmittedReport",
				                                    	alertforPhotos = "alertforPhotos",
				                                    	cameraRoll = "cameraRoll",
				                                    	photoName = "photoName",
				                                    	OK = "OK";
	}
}

