package lib.dataObject;


import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class LoginData {
	
	public LoginData() throws Exception {

	}


	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "LoginDataXml";

	public final String LoginNumber = xml.read(ConsumerTag.LoginNumber, FILEPATH);
																					
																									
	public static class ConsumerTag {

		public static final String LoginNumber = "LoginNumber";
	}



}
