package lib.dataObject;

import java.util.Random;

import lib.dataObject.ExpenseData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class ExpenseData {
	
	public ExpenseData() throws Exception {

	}


	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "ExpenseDataXml";

	public final String JobNumber = xml.read(ConsumerTag.JobNumber, FILEPATH),
			                         EmployeeName = xml.read(ConsumerTag.EmployeeName, FILEPATH),
			                        	VehicleNumber = xml.read(ConsumerTag.VehicleNumber, FILEPATH),
			                        	TotalAmount = xml.read(ConsumerTag.TotalAmount, FILEPATH),
			                        	Description = xml.read(ConsumerTag.Description, FILEPATH),
			                        	AlertForSubmit = xml.read(ConsumerTag.AlertForSubmit, FILEPATH);
												
																									
	public static class ConsumerTag {

		public static final String JobNumber = "JobNumber",
				                                   EmployeeName = "EmployeeName",
				                                	  VehicleNumber = "VehicleNumber",
				                                	  TotalAmount = "TotalAmount",
				                                 	Description = "Description",
				                                 	AlertForSubmit = "AlertForSubmit";
		
	}


}
