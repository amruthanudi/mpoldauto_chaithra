package lib.page;



import static org.testng.Assert.assertEquals;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateExpense.Locators;
import qaframework.custom.WaitForElement;
import qaframework.WebElement.WebTimeConstant;
import qaframework.Configuration.Config_MobileAndWeb;



public class ExpensePage {
	
	WaitForElement wait;
	IOSDriver<MobileElement> driver;

	public ExpensePage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}

	public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function select type of expense in create page Date :
	 * 23-April-2017 Author : chaithra
	 **********************************************************************************/
	public ExpensePage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function select type of expense in create page Date :
	 * 23-April-2017 Author : chaithra
	 **********************************************************************************/
	public ExpensePage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	
	/**********************************************************************************
	 * Description : This function enter value in expense Date : 23-April-2017 Author :
	 * chaithra
	 **********************************************************************************/
	public ExpensePage enterValueInExpense(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in expense Date : 23-April-2017 Author :
	 * chaithra
	 **********************************************************************************/
	public ExpensePage enterValueInExpenseFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :23-April-2017
	 * Author : chaithra
	 **********************************************************************************/
	public ExpensePage takePicture() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Locators.cameraBtn)));
        driver.findElement(By.xpath(Locators.cameraBtn)).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("PhotoCapture")));
        driver.findElement(By.name("PhotoCapture")).click();
        driver.findElement(By.name("Use Photo")).click();
       return this;
	}
	
	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :23-April-2017
	 * Author : chaithra
	 **********************************************************************************/
	public ExpensePage clickSave() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.save)));
        driver.findElement(By.name(Locators.save)).click();
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 23-April-2017
	 * Author : chaithra
	 **********************************************************************************/
	public ExpensePage selectDateByScrollingDate(String locator) throws Exception {

      String startx = driver.findElement(By.xpath(locator)).getId();

      JavascriptExecutor js = (JavascriptExecutor) driver;
      HashMap<String, String> scrollObject = new HashMap<String, String>();
      scrollObject.put("direction", "down");
      scrollObject.put("element", startx);
       js.executeScript("mobile: scroll", scrollObject);
       
       return this;

	  }
	
	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 23-April-2017
	 * Author : chaithra
	 **********************************************************************************/
	public ExpensePage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }
	
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 22-Dec-2017 Author : Pushpa
	 **********************************************************************************/
	public ExpensePage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.alertSubmittedReport)));
		driver.findElement(By.name(Locators.alertSubmittedReport)).isDisplayed();
		String actual = driver.findElement(By.name(Locators.alertSubmittedReport)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	

}
