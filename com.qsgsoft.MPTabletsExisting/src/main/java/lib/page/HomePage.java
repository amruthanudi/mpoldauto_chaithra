package lib.page;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.css.sac.Locator;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import lib.locators.CreateExpense.Locators;

import qaframework.Configuration.Config_MobileAndWeb;
import qaframework.WebElement.WebTimeConstant;
import lib.locators.CreateFuel.FuelLocators;
import lib.locators.CreateJobHazard.JobHazardLocators;

public class HomePage {

	
	IOSDriver<MobileElement> driver;

	public HomePage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
	}

	public HomePage clickExpense() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.expenseIcon)));
         driver.findElement(By.name(Locators.expenseIcon)).click();
		 return this;
	}
	
	public HomePage clickFuel() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(FuelLocators.fuelIcon)));
		driver.findElement(By.name(FuelLocators.fuelIcon)).click();
		return this;
	}
	
	public HomePage clickJobHazard() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(JobHazardLocators.jobHazardIcon)));
		driver.findElement(By.name(JobHazardLocators.jobHazardIcon)).click();
		return this;
	}


}
