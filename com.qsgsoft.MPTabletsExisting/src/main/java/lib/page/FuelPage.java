package lib.page;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.HideKeyboardStrategy;
import lib.locators.CreateExpense.Locators;
import lib.locators.CreateFuel.FuelLocators;
import qaframework.custom.WaitForElement;
import qaframework.WebElement.WebTimeConstant;
import qaframework.Configuration.Config_MobileAndWeb;

public class FuelPage {
	
	WaitForElement wait;
	IOSDriver<MobileElement> driver;
	
	public FuelPage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}

	public WebDriver wdriver = Config_MobileAndWeb.wdriver;
	
	/**********************************************************************************
	 * Description : This function select type of expense in create page Date :
	 * 23-April-2017 Author : chaithra
	 **********************************************************************************/
	public FuelPage clickElementByName(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function select type of Fuel in create page
	 *  Date :25-April-2017
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public FuelPage clickElementByxpath(String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		 return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Fuel
	 *  Date : 25-April-2017
	 *   Author : Ashwini Acharya
	 **********************************************************************************/
	public FuelPage enterValueInFuel(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locators)));
         driver.findElement(By.name(locators)).click();
		driver.findElement(By.name(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter value in Fuel
	 *  Date : 24-April-2017 
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public FuelPage enterValueInFuelFromxpath(String CrewName, String locators) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locators)));
         driver.findElement(By.xpath(locators)).click();
		driver.findElement(By.xpath(locators)).sendKeys(CrewName);
		driver.hideKeyboard(HideKeyboardStrategy.PRESS_KEY, "return");
		return this;
	}

	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :24-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public FuelPage takePicture() throws Exception {	
		
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FuelLocators.cameraButton)));
        driver.findElement(By.xpath(FuelLocators.cameraButton)).click(); 
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("PhotoCapture")));
        driver.findElement(By.name("PhotoCapture")).click();
        driver.findElement(By.name("Use Photo")).click();
       return this;
	}		

		
	public FuelPage takePhotos() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FuelLocators.photoButton)));
        driver.findElement(By.xpath(FuelLocators.photoButton)).click();         
        driver.findElement(By.name(FuelLocators.cameraRoll)).click();
        driver.findElement(By.name(FuelLocators.photoName)).click();
        return this;
	}		

	
	/**********************************************************************************
	 * Description : This function take a picture using camera
	 * Date :24-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public FuelPage clickSave() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(FuelLocators.saveButton)));
        driver.findElement(By.name(FuelLocators.saveButton)).click();
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function select date  by scrolling date
	 * Date : 23-April-2017
	 * Author : chaithra
	 **********************************************************************************/
	public FuelPage selectDateByScrollingDate(String locator) throws Exception {

      String startx = driver.findElement(By.xpath(locator)).getId();

      JavascriptExecutor js = (JavascriptExecutor) driver;
      HashMap<String, String> scrollObject = new HashMap<String, String>();
      scrollObject.put("direction", "down");
      scrollObject.put("element", startx);
       js.executeScript("mobile: scroll", scrollObject);
       
       return this;

	  }

	/**********************************************************************************
	 * Description : This function select date in by scrolling
	 * Date : 24-April-2017
	 * Author : Ashwini Acharya
	 **********************************************************************************/
	public FuelPage clickSubmit(String locator) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
        driver.findElement(By.name(locator)).click();
		return this;

	  }	
		
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 22-Dec-2017 Author : Pushpa
	 **********************************************************************************/
	public FuelPage verReportIsSubmitted(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(FuelLocators.alertForSubmittedReport)));
		driver.findElement(By.name(FuelLocators.alertForSubmittedReport)).isDisplayed();
		String actual = driver.findElement(By.name(FuelLocators.alertForSubmittedReport)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 27-Aprill-2017 Author : Ashwini
	 **********************************************************************************/
	public FuelPage okButtonTappedForAlertSubmission(String msg) throws Exception {
		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(FuelLocators.OK)));
		driver.findElement(By.name(FuelLocators.OK)).click();
		return this;		
	}
	
	/**********************************************************************************
	 * Description : This function used to verify alert msg for submitted report :
	 * 22-Dec-2017 Author : Pushpa
	 **********************************************************************************/
	public FuelPage verifyCameraAlert(String msg) throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(FuelLocators.alertforPhotos)));
		driver.findElement(By.name(FuelLocators.alertforPhotos)).isDisplayed();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(FuelLocators.OK)));
		driver.findElement(By.name(FuelLocators.OK)).click();
		String actual = driver.findElement(By.name(FuelLocators.alertforPhotos)).getText();
		System.out.println("receive" + actual);
		assertEquals(actual, msg);
		return this;
	}	
	
}
